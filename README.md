# SPAC3D Viewer

Check it out in [Production](https://spac3d.app/tolix/chair)!

Or view [Production server Catalog](https://oddpilot.com/catalog.html)

Dev server status: [![Netlify dev server status](https://api.netlify.com/api/v1/badges/52c75659-a5ba-4f89-b4dd-61202d5baaa4/deploy-status)](https://app.netlify.com/sites/pensive-lalande-75097b/deploys)

[Dev server catalog](https://app.netlify.com/sites/pensive-lalande-75097b/dev-catalog)


## Folder Structure

Version controlled folders

- content: Contains each viewer page's metadata in JSON. Pages get built based on this info.
- data: Metadata for website
- static: 3D models, images, etc. That would usually be served over a CDN
- src: React components and page templates

Generated folders
- public: Buildtime-generated folder that Gatsby builds 
- node_modules: npm-generated folder containing JS libraries and modules

## Screens

- SPAC3D Viewer `templates/viewer.jsx` (3D, has lots of functions)
- Android model-viewer `templates/viewer-android.jsx` (3D, staging area for AR in Android in browser)
- Scene Viewer 3D (3D, Android's native 3D viewer)
- Scene Viewer AR (AR, Android's native AR viewer)
- QuickLook 3D (3D, Android's native 3D viewer)
- QuickLook AR (AR, Android's native AR viewer)

WARNING: Make sure to edit `static/robots.txt` to include your domain for the sitemap!

## 오류보고 양식
납품 업체로부터 버그가 발생했다는 제보를 받을 경우, 다음 양식을 반드시 채워주십사 부탁합니다. 기초적인 정보 없이는 버그 재현 자체가 불가능해 디버깅을 진행할 수 없습니다.

- 사용 핸드폰 기종:
- 운영체제 버전:
- ARCore/Kit 버전:
- 사용 브라우저:
- 증상 설명:
- 해당 증상이 생긴 가구 모델명:
- 해당 증상이 발생한 단계:


각각에 대한 세부적인 설명은 아래와 같습니다.
- 안드로이드 운영체제 확인: 설정 -> 휴대전화 정보 -> 소프트웨어 정보 -> 안드로이드 버전에서 확인. 
- 아이폰 운영체제 확인: Settings -> General -> About -> Software Version
- 브라우저는 다음과 같이 적습니다. ex) 크롬, 삼성인터넷, 네이버/카카오/etc 인앱 브라우저
- 가구 모델은 다음과 같이 적습니다. ex) IROAL004

증상 발생 단계는 다음 3개중 하나로 추려주세요. 
1. 스페이스드 3D 뷰어 단계 
2. AR 프리뷰 단계 
3. AR 뷰 단계