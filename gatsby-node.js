const glob = require('glob');

const getDirectories = (src, cb) => {
  glob(`${src}/**/*.json`, cb);
};

exports.createPages = ({ actions: { createPage } }) => {
  getDirectories('content', (err, res) => {
    if (err) {
      console.log('Error', err);
    } else {
      // TODO: 순서 상관 없어서 비동기로 처리해도 됨.
      for (let i = 0; i < res.length; i+=1) {
        const product = require(`./${res[i]}`);

        // create a spac3d viewer page for each json file
        createPage({
          path: `/${product.brandCode}/${product.productCode}`,
          component: require.resolve('./src/templates/viewer.jsx'),
          context: product,
        });

        // create a <model-viewer> page for each json file
        createPage({
          path: `/${product.brandCode}/${product.productCode}/android`,
          component: require.resolve('./src/templates/viewer-android.jsx'),
          context: product,
        });

        // create a <model-viewer> page for each color configuration
        // TODO elegantly remove duplicates: '/tolix/chair/android' and '/tolix/chair/android/model'
        if (product.hasMultipleColors) {
          product.modelConfigs.forEach(colorConfig => {
            createPage({
              path: `/${product.brandCode}/${product.productCode}/android/${colorConfig.colorCode}`,
              component: require.resolve('./src/templates/viewer-android-color.jsx'),
              context: {...product, colorCode: colorConfig.colorCode},
            });
          });
        }
      }
    }
  });
};
