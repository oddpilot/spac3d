import React, { Component } from 'react';
import './Header.css';

class Header extends Component {
  render() {
    const { name, brand } = this.props;
    return (
      <header>
        <div className="viewer-logo">
          <a href="https://oddpilot.com" target="_blank" rel="noopener noreferrer">
            <img src="/img/viewer/viewer-logo.png" alt="Logo of SPAC3D" />
          </a>
        </div>
        <div className="product-info">
          <h1 className="product-name">{name}</h1>
          <h2 className="product-brand">{brand}</h2>
        </div>
      </header>
    );
  }
}

export default Header;
