import React, { Component } from 'react';
import PXLoadingManager from './loadingManager';
import './LoadingBar.css';

class LoadingBar extends Component {
  render() {
    return (
      <div id="loading-overlay">
        <div id="loading-bar" className="loading-bar animate">
          <span id="progress">
            <span />
          </span>
        </div>
      </div>
    );
  }
}

// Is this a named export?
export { LoadingBar, PXLoadingManager };
