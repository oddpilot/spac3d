import { LoadingManager } from 'three'; // https://github.com/mrdoob/three.js/blob/master/src/loaders/LoadingManager.js

const PXLoadingManager = function PXLoadingManager() {
  const progressBar = document.querySelector('#progress');
  const loadingOverlay = document.querySelector('#loading-overlay');
  const loadingManager = new LoadingManager();
  const _ = loadingManager;

  _.loadingBarUpdateAmount = 1.0;
  _.loadingPercentDefault = 5.0;
  _.loadingFrameID = null;

  _.onStart = function onStart() {
    _.loadingPercentComplete = _.loadingPercentDefault;
    if (loadingOverlay != null) {
      loadingOverlay.classList.remove('loading-overlay-hidden');
    }
    if (_.loadingFrameID !== null) return;
    _.animateLoadingBar();
  };

  _.animateLoadingBar = function animateLoadingBar() {
    _.loadingPercentComplete = (_.loadingPercentComplete + _.loadingBarUpdateAmount) % 100;
    if (progressBar != null) {
      progressBar.style.width = `${_.loadingPercentComplete}%`;
    }
    _.loadingFrameID = requestAnimationFrame(_.animateLoadingBar);
  };

  _.onLoad = function onLoad() {
    if (loadingOverlay != null) {
      loadingOverlay.classList.add('loading-overlay-hidden');
    }
    if (progressBar != null) {
      progressBar.style.width = _.loadingPercentDefault;
    }
    cancelAnimationFrame(_.loadingFrameID);
    _.loadingFrameID = null;
  };

  _.onError = function onError(e) {
    console.error(e);
    if (progressBar != null) {
      progressBar.style.backgroundColor = 'red';
    }
  };

  return _; // expose loadingManager
};

export default PXLoadingManager;
