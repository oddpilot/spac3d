import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import './Modals.css';
import config from '../../../data/SiteConfig';

class Modals extends Component {
  state = {
    pageUrl: '',
    thumbnailUrl: '',
  };

  componentDidMount() {
    const { productCode, brandCode } = this.props;

    // looks like https://spac3d.app/tolix/chair
    this.setState({
      pageUrl: `${config.siteUrl}/${brandCode}/${productCode}`,
      thumbnailUrl: `${config.siteUrl}/img/thumbnails/${brandCode}/${productCode}.png`,
    });
  }

  shareOnKakao = () => {
    const { productName, brandName } = this.props;
    const { thumbnailUrl, pageUrl } = this.state;
    const { Kakao } = window;
    Kakao.init(config.kakaoJavaScriptKey);
    Kakao.Link.sendDefault({
      objectType: 'feed',
      content: {
        title: productName,
        description: `${productName} - ${brandName}`,
        // 이미지 사이즈 526 * 528
        imageUrl: thumbnailUrl,
        link: {
          mobileWebUrl: pageUrl,
          webUrl: pageUrl,
        },
      },
      buttons: [
        {
          title: '브라우저에서 보기',
          link: {
            mobileWebUrl: pageUrl,
            webUrl: pageUrl,
          },
        },
      ],
    });
  };

  render() {
    const {
      isWarningModalOpen,
      isQrModalOpen,
      isShareModalOpen,
      toggleWarningModal,
      toggleQrModal,
      toggleShareModal,
    } = this.props;
    const { pageUrl } = this.state;

    return (
      <section>
        {/* <!-- 뷰어 공유 팝업 --> */}
        <div
          id="share-modal"
          className="share-modal"
          role="presentation"
          style={{ visibility: isShareModalOpen ? 'visible' : 'hidden' }}
          aria-hidden={!isShareModalOpen}
        >
          <div className="share-modal-content">
            <span className="close" onClick={toggleShareModal} role="button" tabIndex="0">
              &times;
            </span>
            <p>3D 뷰어 공유하기</p>

            {/* <!-- 공유 버튼 --> */}
            <div id="social-btn-group">
              <a
                className="social-btn"
                href={`https://twitter.com/intent/tweet?text=Come%20see%20our%20product!&url=${encodeURI(pageUrl)}`}
                rel="noopener noreferrer"
                target="_blank"
              >
                {/* <Helmet>
                  <script>
                    {`
                    window.twttr = (function (d, s, id) {
                      var js = null,
                      fjs = d.getElementsByTagName(s)[0],
                      t = window.twttr || {};
                      if (d.getElementById(id)) return t;
                      js = d.createElement(s);
                      js.id = id;
                      js.src = 'https://platform.twitter.com/widgets.js';
                      fjs.parentNode.insertBefore(js, fjs);
                      t._e = [];
                      t.ready = function (f) {
                        t._e.push(f);
                      };
                      return t;
                    })(document, 'script', 'twitter-wjs');
                  `}
                  </script>
                </Helmet> */}
                <img src="/img/viewer/twitter-icon.png" alt="Icon for sharing to twitter" />
              </a>
              <div className="social-btn" data-href={pageUrl} data-layout="button" data-size="small">
                <a
                  className="fb-xfbml-parse-ignore"
                  href={`https://www.facebook.com/sharer/sharer.php?u=${encodeURI(pageUrl)}&amp;src=sdkpreparse`}
                  rel="noopener noreferrer"
                  target="_blank"
                >
                  <img src="/img/viewer/facebook-icon.png" alt="Icon for sharing to Facebook" />
                </a>
              </div>
              <button id="kakao-link-btn" type="button" className="social-btn" onClick={this.shareOnKakao}>
                <Helmet>
                  <script src="https://developers.kakao.com/sdk/js/kakao.min.js" />
                </Helmet>
                <img src="/img/viewer/kakao-icon.png" alt="Icon for sharing to Kakaotalk messenger" />
              </button>
            </div>
            <hr />

            {/* <!-- iframe 첨부 링크 복사 칸 --> */}
            <div className="flex-container">
              <p className="flex-item">3D 뷰어 첨부하기</p>
              <a className="flex-item" href="/viewer-help" target="_blank">
                첨부 방법
              </a>
            </div>
            <input
              id="iframe-box"
              type="text"
              name=""
              readOnly
              value={`<iframe title="3D/AR model viewer" name="spac3d" style="min-height:450px;min-width:300px;width:640px;height:360px;" src="${pageUrl}" frameborder="0"></iframe>`}
            />
          </div>
        </div>

        {/* <!-- 카카오톡 AR 경고 팝업 --> */}

        <div
          id="warningModal"
          className="warningModal"
          style={{ visibility: isWarningModalOpen ? 'visible' : 'hidden' }}
          aria-hidden={!isWarningModalOpen}
        >
          <div className="warningModal__container">
            <h1 className="warningModal__title">
              AR 기능을 사용하시려면 아이폰인 경우 사파리, 안드로이드인 경우 크롬 브라우저를 이용해주세요.
            </h1>
            <button id="warningModalBtn" type="button" className="warningModal__btn" onClick={toggleWarningModal}>
              확인
            </button>
          </div>
        </div>

        {/* <!-- Desktop QR 코드 팝업 --> */}
        <div
          id="qrModal"
          className="qrModal"
          style={{ visibility: isQrModalOpen ? 'visible' : 'hidden' }}
          aria-hidden={!isQrModalOpen}
        >
          <div className="qrModal__container">
            <h1 className="qrModal__title">스마트폰으로 아래 QR을 스캔해서 증강현실 기능을 사용해보세요.</h1>
            <img
              className="qrModal__qr"
              alt="QR code that can be scanned to open this page in a smartphone"
              src={`https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=${pageUrl}`}
            />
            <button id="qrModalBtn" type="button" className="qrModal__btn" onClick={toggleQrModal}>
              확인
            </button>
          </div>
        </div>
      </section>
    );
  }
}

export default Modals;
