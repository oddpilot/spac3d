import React, { Component } from 'react';
import './ARViewButton.css';

class ARViewButton extends Component {
  state = {
    userAgentInfo: {
      IS_IOS: false,
      IS_IOS_13: false,
      IS_ANDROID: false,
      IS_MOBILE: true,
      IS_KAKAO: false,
      IS_NAVER: false,
      IS_UCBROWSER: false,
      IS_FIREFOX: false,
      IS_CHROME: false,
      IS_SAFARI: false,
    },
  };

  componentDidMount() {
    this.setState({ userAgentInfo: this.getUserAgentInfo() });
  }

  getUserAgentInfo = () => {
    // 카카오 인앱 브라우저 및 운영체제에 따른 분기 설정
    const IS_IOS = /iPad|iPhone|iPod/.test(window.navigator.userAgent) && !window.MSStream;
    const IS_IOS_13 = /OS 13/.test(window.navigator.userAgent) && !window.MSStream && IS_IOS;
    const IS_ANDROID = /Android/.test(window.navigator.userAgent) && !window.MSStream;
    const IS_MOBILE = (/Mobile/.test(window.navigator.userAgent) && !window.MSStream) || IS_IOS || IS_ANDROID;
    const IS_KAKAO = /KAKAOTALK|KAKAOSTORY/.test(window.navigator.userAgent) && !window.MSStream;
    const IS_NAVER = /NAVER/.test(window.navigator.userAgent) && !window.MSStream;
    const IS_UCBROWSER = /UCBrowser/.test(window.navigator.userAgent) && !window.MSStream;
    const IS_SAMSUNGBROWSER = /Samsung/.test(window.navigator.userAgent) && !window.MSStream;
    const IS_FIREFOX = /Firefox|FxiOS/.test(window.navigator.userAgent) && !window.MSStream;
    const IS_CHROME =
      /Chrome|CriOS/.test(window.navigator.userAgent) &&
      !window.MSStream &&
      !IS_UCBROWSER &&
      !IS_SAMSUNGBROWSER &&
      !IS_NAVER &&
      !IS_KAKAO;
    const IS_SAFARI =
      /Safari/.test(window.navigator.userAgent) && !window.MSStream && !IS_CHROME && !IS_FIREFOX && !IS_SAMSUNGBROWSER;

    return {
      IS_IOS,
      IS_IOS_13,
      IS_ANDROID,
      IS_MOBILE,
      IS_KAKAO,
      IS_NAVER,
      IS_UCBROWSER,
      IS_FIREFOX,
      IS_CHROME,
      IS_SAFARI,
    };
  };

  render() {
    const { userAgentInfo: UA } = this.state;
    const { productCode, brandCode, modelConfigs, toggleQrModal, toggleWarningModal } = this.props;
    if (
      UA.IS_KAKAO ||
      UA.IS_NAVER ||
      UA.IS_UCBROWSER ||
      (UA.IS_IOS && !UA.IS_SAFARI) ||
      (UA.IS_ANDROID && !UA.IS_CHROME)
    ) {
      // Opens warning modal
      return (
        <button className="btn-right" id="ar-btn-unsupported" type="button" onClick={toggleWarningModal}>
          <img src="/img/viewer/vr-icon.png" alt="Icon to open in unsupported browser AR mode" />
        </button>
      );
    }
    if (UA.IS_IOS && UA.IS_SAFARI) {
      return (
        <a
          className="btn-right"
          id="ar-btn-ios"
          href={
            UA.IS_IOS_13
              ? `/models/${brandCode}/${productCode}/model-ios13.usdz`
              : `/models/${brandCode}/${productCode}/model.usdz`
          }
          rel="ar"
        >
          <img src="/img/viewer/vr-icon.png" alt="Icon to open in iOS AR mode" />
        </a>
      );
    }
    if (UA.IS_ANDROID && UA.IS_CHROME) {
      return (
        <a className="btn-right" id="ar-btn-and" href={`/${brandCode}/${productCode}/android`}>
          <img src="/img/viewer/vr-icon.png" alt="Icon to open in Android AR mode" />
        </a>
      );
    }
    if (!UA.IS_MOBILE) {
      // opens QR modal
      return (
        <button className="btn-right" id="ar-btn-desktop" type="button" onClick={toggleQrModal}>
          <img src="/img/viewer/vr-icon.png" alt="Icon to open in Desktop AR mode" />
        </button>
      );
    }
    // 우리가 모르는 플랫폼인 경우
    // Opens warning modal
    return (
      <button className="btn-right" id="ar-btn-unsupported" type="button" onClick={toggleWarningModal}>
        <img src="/img/viewer/vr-icon.png" alt="Icon to open in unsupported browser AR mode" />
      </button>
    );
  }
}

export default ARViewButton;
