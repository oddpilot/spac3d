import React, { Component } from 'react';
import './ColorSelector.css';

class ColorSelector extends Component {
  componentDidMount() {
    // TODO: Change the code below to use Reactic style. Use props instead of hard coded colors
    const btnContainer = document.getElementById('color-btn-group');
    const btns = btnContainer.getElementsByClassName('color-btn');
    const colors = ['#61CAED', '#AAF0D1', '#FFBCC6', '#FFFFFF', '#CDCDCD'];
    // Loop through the buttons and add the active class to the current/clicked button
    for (let i = 0; i < btns.length; i += 1) {
      btns[i].style.backgroundColor = colors[i];
    }
  }

  changeModel = i => {
    return event => {
      const { brandCode, productCode, modelConfigs, setModelAttributes } = this.props;
      const { colorCode, hexColor, metalness, roughness } = modelConfigs[i];

      setModelAttributes(hexColor, metalness, roughness);

      // Get Buttons from DOM
      const arQuickLookBtn = document.getElementById('ar-btn-ios');
      if (arQuickLookBtn) {
        const IS_IOS_13 = /OS 13/.test(window.navigator.userAgent); // && !window.MSStream && IS_IOS conditions are not needed here
        if (IS_IOS_13) {
          arQuickLookBtn.href = `/models/${brandCode}/${productCode}/${colorCode}-ios13.usdz`;
        } else {
          arQuickLookBtn.href = `/models/${brandCode}/${productCode}/${colorCode}.usdz`;
        }
      }

      const arAndBtn = document.getElementById('ar-btn-and');
      if (arAndBtn) {
        arAndBtn.href = `/${brandCode}/${productCode}/android/${colorCode}`;
      }

      // Change color selector button UI
      // TODO this should be moved to ColorButton.jsx
      const current = document.getElementsByClassName('active');
      const { target: next } = event;
      if (current && current.length) {
        current[0].className = current[0].className.replace(' active', '');
      }
      next.className += ' active';
    };
  };

  render() {
    // TODO: change the return statement to a for loop using props
    return (
      <section className="btn-group-left" id="color-btn-group">
        {/* // <!-- 색 선택 버튼: 연하늘, 민트, 핑크, 화이트, 메탈 순. --> */}
        {/* <!-- 기본값은 연하늘로 설정되어 있음 --> */}
        <button type="button" className="color-btn active" onClick={this.changeModel(0)} />
        <button type="button" className="color-btn" onClick={this.changeModel(1)} />
        <button type="button" className="color-btn" onClick={this.changeModel(2)} />
        <button type="button" className="color-btn" onClick={this.changeModel(3)} />
        <button type="button" className="color-btn" onClick={this.changeModel(4)} />
      </section>
    );
  }
}

export default ColorSelector;
