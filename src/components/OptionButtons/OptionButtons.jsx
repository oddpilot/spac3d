import React, { Component } from 'react';
import ARViewButton from './ARViewButton/ARViewButton';
import './OptionButtons.css';

class OptionButtons extends Component {
  render() {
    const { productCode, brandCode, modelConfigs, toggleShareModal, toggleQrModal, toggleWarningModal } = this.props;
    return (
      <section className="btn-group-right">
        {/* // <!-- 오른쪽 버튼: 공유, 새 창에서 보기, AR 보기 --> */}
        <button className="btn-right" id="share-btn" type="button" onClick={toggleShareModal}>
          <img src="/img/viewer/share-icon.png" alt="Icon for sharing to various Social Media" />
        </button>
        <a
          className="btn-right"
          id="expand-btn"
          href={`/${brandCode}/${productCode}`}
          target="_blank"
          rel="noopener noreferrer"
        >
          <img
            src="/img/viewer/expand-icon.png"
            alt="Icon for opening a full-screen view of this page"
          />
        </a>

        <ARViewButton
          productCode={productCode}
          brandCode={brandCode}
          modelConfigs={modelConfigs}
          toggleQrModal={toggleQrModal}
          toggleWarningModal={toggleWarningModal}
        />
      </section>
    );
  }
}

export default OptionButtons;
