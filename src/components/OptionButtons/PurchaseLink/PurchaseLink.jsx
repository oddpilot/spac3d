import React, { Component } from 'react';
import './PurchaseLink.css';

class PurchaseLink extends Component {
  render() {
    const { purchaseURL } = this.props;
    return (
      <a href={purchaseURL} target="_blank" rel="noopener noreferrer" id="buy-btn">
        {/* // <!-- 제품 구매하기 버튼 TODO: 색 연하늘에 맞게 바꾸어야할 듯.--> */}
        구매하기
      </a>
    );
  }
}

export default PurchaseLink;
