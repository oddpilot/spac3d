import React, { Component } from 'react';
import { LoadingBar, PXLoadingManager } from '../LoadingBar/LoadingBar';
import Header from '../Header/Header';
import ColorSelector from '../OptionButtons/ColorSelector/ColorSelector';
import PurchaseLink from '../OptionButtons/PurchaseLink/PurchaseLink';
import OptionButtons from '../OptionButtons/OptionButtons';
import Modals from '../Modals/Modals';

import { create3DScene, setModelAttributes, toggleControls } from './main';
import './Viewer.css';

class Viewer extends Component {
  state = {
    isShareModalOpen: false,
    isWarningModalOpen: false,
    isQrModalOpen: false,
  };

  componentDidMount() {
    const { product } = this.props;
    create3DScene(PXLoadingManager, product);
    document.body.classList.add('viewer-body');
  }

  componentWillUnmount() {
    document.body.classList.remove('viewer-body');
  }

  toggleShareModal = () => {
    const { isShareModalOpen } = this.state;
    this.setState({ isShareModalOpen: !isShareModalOpen });
    toggleControls();
  };

  toggleWarningModal = () => {
    const { isWarningModalOpen } = this.state;
    this.setState({ isWarningModalOpen: !isWarningModalOpen });
  };

  toggleQrModal = () => {
    const { isQrModalOpen } = this.state;
    this.setState({ isQrModalOpen: !isQrModalOpen });
  };

  render() {
    const { product } = this.props;
    const { isShareModalOpen, isQrModalOpen, isWarningModalOpen } = this.state;
    return (
      <article>
        <Header name={product.productName} brand={product.brandName} />
        <LoadingBar />

        {/* Only render the ColorSelector in Tolix page */}
        {/* TODO do we really need a boolean flag? */}
        {product.hasMultipleColors && (
          <ColorSelector
            productCode={product.productCode}
            brandCode={product.brandCode}
            modelConfigs={product.modelConfigs}
            setModelAttributes={setModelAttributes}
          />
        )}

        {product.purchaseURL && <PurchaseLink purchaseURL={product.purchaseURL} />}

        <OptionButtons
          productCode={product.productCode}
          brandCode={product.brandCode}
          modelConfigs={product.modelConfigs}
          toggleShareModal={this.toggleShareModal}
          toggleQrModal={this.toggleQrModal}
          toggleWarningModal={this.toggleWarningModal}
        />
        <Modals
          productName={product.productName}
          productCode={product.productCode}
          brandName={product.brandName}
          brandCode={product.brandCode}
          isShareModalOpen={isShareModalOpen}
          toggleShareModal={this.toggleShareModal}
          isQrModalOpen={isQrModalOpen}
          toggleQrModal={this.toggleQrModal}
          isWarningModalOpen={isWarningModalOpen}
          toggleWarningModal={this.toggleWarningModal}
        />
      </article>
    );
  }
}

export default Viewer;
