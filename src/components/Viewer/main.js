import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader';

let object; // 3D 모델을 담기 위한 변수
let controls; // 사용자와 3D 화면의 인터랙션을 관장하는 설정

// deferred Promise pattern that will resolve only upon finishing model load
const Deferred = f => {
  const fn = f || (() => {});

  let res = null;
  let rej = null;

  const promise = new Promise((resolve, reject) => {
    res = resolve;
    rej = reject;
    fn(resolve, reject);
  });

  promise.resolve = val => {
    if (val === undefined) {
      res();
    } else {
      res(val);
    }
    return promise; // for chainability
  };
  promise.reject = reason => {
    if (reason === undefined) {
      rej();
    } else {
      rej(reason);
    }
    return promise; // for chainability
  };
  promise.promise = () => promise.then(); // to derive an undecorated promise (expensive but simple).

  return promise;
};
const modelLoaded = Deferred();

const create3DScene = (LoadingManager, product) => {
  // 3D 뷰어 시작
  const container = document.createElement('div');
  const scene = new THREE.Scene();
  const cameraSettings = product.camera;
  const camera = new THREE.PerspectiveCamera(
    cameraSettings.fov,
    window.innerWidth / window.innerHeight,
    cameraSettings.near,
    cameraSettings.far // kffic.cameraSettings.far == 100
  );
  let renderer;

  const loader = new GLTFLoader(LoadingManager());
  const dracoLoader = new DRACOLoader();
  dracoLoader.setDecoderPath('/js/draco/');
  loader.setDRACOLoader(dracoLoader);

  const envFormat = '.png';
  const inPath = '/img/viewer/envMap/lightroom/';
  const inUrls = [
    `${inPath}px${envFormat}`,
    `${inPath}nx${envFormat}`,
    `${inPath}py${envFormat}`,
    `${inPath}ny${envFormat}`,
    `${inPath}pz${envFormat}`,
    `${inPath}nz${envFormat}`,
  ];
  const inEnvMap = new THREE.CubeTextureLoader().load(inUrls);

  const animate = () => {
    requestAnimationFrame(animate);
    controls.update();
    renderer.render(scene, camera);
  };

  // TODO: Shadow 까지 포함한 모델로 불러오기
  const loadShadow = () => {
    const {
      brandCode,
      productCode,
      shadow: { size, format, position },
    } = product;
    const planeGeometry = new THREE.PlaneGeometry(size.x, size.y);
    planeGeometry.rotateX(-Math.PI / 2);
    const texture = new THREE.TextureLoader().load(
      `/models/${brandCode}/${productCode}/shadow${format}`
    );
    const planeMaterial = new THREE.MeshBasicMaterial({
      map: texture,
      transparent: true,
    });
    const plane = new THREE.Mesh(planeGeometry, planeMaterial);
    plane.position.set(position.x, position.y, position.z);
    scene.add(plane);
  };

  const loadModel = () => {
    const {
      brandCode,
      productCode,
      model: { format, scale },
      shadow,
    } = product;
    loader.load(`/models/${brandCode}/${productCode}/model${format}`, gltf => {
      gltf.scene.traverse(mesh => {
        if (mesh instanceof THREE.Mesh) {
          mesh.material.envMap = inEnvMap;
          object = mesh;
          modelLoaded.resolve();
        }
      });
      gltf.scene.scale.set(scale, scale, scale);
      scene.add(gltf.scene);
      if (shadow.external) {
        loadShadow();
      }
    });
  };

  const initBackground = () => {
    scene.background = new THREE.Color(14869218);
  };

  const initCamera = () => {
    const { position } = cameraSettings;
    camera.position.set(position.x, position.y, position.z);
    scene.add(camera);
  };

  const initControl = () => {
    const { control } = product;
    const { target } = control;
    controls.maxDistance = control.max;
    controls.minDistance = control.min;
    controls.target.set(target.x, target.y, target.z);
    controls.enableDamping = true;
    controls.enablePan = false;
    controls.dampingFactor = 0.125;
    controls.rotateSpeed = 0.6;
  };

  const initLight = () => {
    // 상품의 빛 설정값을 받아옴
    const { lights } = product;

    // 전체 화면 조도 조절을 위한 자연광 설정 (exposure)
    const ambient = new THREE.AmbientLight(0xffffff, lights.ambientIntensity);
    scene.add(ambient);

    // 모델 비추기 위한 포인트 라이트 3개 설정
    const backLight = new THREE.PointLight(0xffffff, lights.backLightIntensity, 0, 2);
    const rightLight = new THREE.PointLight(0xffffff, lights.rightLightIntensity, 0, 2);
    const frontLight = new THREE.PointLight(0xffffff, lights.frontLightIntensity, 0, 2);

    backLight.position.set(0, 18.56, -24);
    rightLight.position.set(20.7846, 18.56, -12);
    frontLight.position.set(-20.7846, 18.56, 12);
    scene.add(backLight);
    scene.add(rightLight);
    scene.add(frontLight);
  };

  const initRenderer = () => {
    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.gammaOutput = true;
    renderer.gammaFactor = 2.2;
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth, window.innerHeight);
    container.appendChild(renderer.domElement);
    controls = new OrbitControls(camera, renderer.domElement);
  };

  const onWindowResize = () => {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
  };

  const onMouseDown = () => {
    renderer.domElement.style.cursor = 'grabbing';
  };

  const onMouseUp = () => {
    renderer.domElement.style.cursor = 'grab';
  };

  const init = function init() {
    document.body.appendChild(container);
    initBackground();
    initCamera();
    initLight();
    loadModel();
    initRenderer();
    initControl();
    window.addEventListener('resize', onWindowResize, false);
    document.addEventListener('mousedown', onMouseDown, false);
    document.addEventListener('mouseup', onMouseUp, false);
  };

  init();
  animate();
};

const toggleControls = () => {
   // expose controls to the modal: opening modals disables viewer controls
  controls.enabled = !controls.enabled;
};

const setModelAttributes = (hexColorString, metalness, roughness) => {
  const hexVal = parseInt(hexColorString, 16);
  modelLoaded.then(() => {
    object.material.color = new THREE.Color(hexVal);
    object.material.metalness = metalness;
    object.material.roughness = roughness;
  });
};

export { create3DScene, toggleControls, setModelAttributes };
