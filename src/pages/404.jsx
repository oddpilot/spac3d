import React from 'react';

const pageNotFound = () => <div>Nothing Found on this Page!</div>;

export default pageNotFound;
