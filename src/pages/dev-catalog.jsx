import React from "react";

const devCatalog = () => {
  return (
    <ul>
      <li>
        <a href="/bplusm/woodbury">/bplusm/woodbury</a>
      </li>
      <li>
        <a href="/bplusm/woodbury/android">/bplusm/woodbury/android</a>
      </li>
      <li>
        <a href="/breville/BES990">/breville/BES990</a>
      </li>
      <li>
        <a href="/breville/BES990/android">/breville/BES990/android</a>
      </li>
      <li>
        <a href="/iroche/IRO01CH023">/iroche/IRO01CH023</a>
      </li>
      <li>
        <a href="/iroche/IRO01CH023/android">/iroche/IRO01CH023/android</a>
      </li>
      <li>
        <a href="/iroche/IRO01FH023">/iroche/IRO01FH023</a>
      </li>
      <li>
        <a href="/iroche/IRO01FH023/android">/iroche/IRO01FH023/android</a>
      </li>
      <li>
        <a href="/iroche/IRO02BK018">/iroche/IRO02BK018</a>
      </li>
      <li>
        <a href="/iroche/IRO02BK018/android">/iroche/IRO02BK018/android</a>
      </li>
      <li>
        <a href="/iroche/IRO02CK018">/iroche/IRO02CK018</a>
      </li>
      <li>
        <a href="/iroche/IRO02CK018/android">/iroche/IRO02CK018/android</a>
      </li>
      <li>
        <a href="/iroche/IRO03BB036">/iroche/IRO03BB036</a>
      </li>
      <li>
        <a href="/iroche/IRO03BB036/android">/iroche/IRO03BB036/android</a>
      </li>
      <li>
        <a href="/iroche/IRO03CB036">/iroche/IRO03CB036</a>
      </li>
      <li>
        <a href="/iroche/IRO03CB036/android">/iroche/IRO03CB036/android</a>
      </li>
      <li>
        <a href="/iroche/IRO04CB010">/iroche/IRO04CB010</a>
      </li>
      <li>
        <a href="/iroche/IRO04CB010/android">/iroche/IRO04CB010/android</a>
      </li>
      <li>
        <a href="/iroche/IRO04DB010">/iroche/IRO04DB010</a>
      </li>
      <li>
        <a href="/iroche/IRO04DB010/android">/iroche/IRO04DB010/android</a>
      </li>
      <li>
        <a href="/iroche/IRO05DB015">/iroche/IRO05DB015</a>
      </li>
      <li>
        <a href="/iroche/IRO05DB015/android">/iroche/IRO05DB015/android</a>
      </li>
      <li>
        <a href="/iroche/IRO05EB015">/iroche/IRO05EB015</a>
      </li>
      <li>
        <a href="/iroche/IRO05EB015/android">/iroche/IRO05EB015/android</a>
      </li>
      <li>
        <a href="/iroche/IRO06AB014">/iroche/IRO06AB014</a>
      </li>
      <li>
        <a href="/iroche/IRO06AB014/android">/iroche/IRO06AB014/android</a>
      </li>
      <li>
        <a href="/iroche/IRO06BH024">/iroche/IRO06BH024</a>
      </li>
      <li>
        <a href="/iroche/IRO06BH024/android">/iroche/IRO06BH024/android</a>
      </li>
      <li>
        <a href="/iroche/IRO07GK017">/iroche/IRO07GK017</a>
      </li>
      <li>
        <a href="/iroche/IRO07GK017/android">/iroche/IRO07GK017/android</a>
      </li>
      <li>
        <a href="/iroche/IRO07NP031">/iroche/IRO07NP031</a>
      </li>
      <li>
        <a href="/iroche/IRO07NP031/android">/iroche/IRO07NP031/android</a>
      </li>
      <li>
        <a href="/iroche/IRO08AH024">/iroche/IRO08AH024</a>
      </li>
      <li>
        <a href="/iroche/IRO08AH024/android">/iroche/IRO08AH024/android</a>
      </li>
      <li>
        <a href="/iroche/IRO08CH024">/iroche/IRO08CH024</a>
      </li>
      <li>
        <a href="/iroche/IRO08CH024/android">/iroche/IRO08CH024/android</a>
      </li>
      <li>
        <a href="/iroche/IRO09BO051">/iroche/IRO09BO051</a>
      </li>
      <li>
        <a href="/iroche/IRO09BO051/android">/iroche/IRO09BO051/android</a>
      </li>
      <li>
        <a href="/iroche/IRO09MO088">/iroche/IRO09MO088</a>
      </li>
      <li>
        <a href="/iroche/IRO09MO088/android">/iroche/IRO09MO088/android</a>
      </li>
      <li>
        <a href="/iroche/IRO10AP033">/iroche/IRO10AP033</a>
      </li>
      <li>
        <a href="/iroche/IRO10AP033/android">/iroche/IRO10AP033/android</a>
      </li>
      <li>
        <a href="/iroche/IRO11BB008">/iroche/IRO11BB008</a>
      </li>
      <li>
        <a href="/iroche/IRO11BB008/android">/iroche/IRO11BB008/android</a>
      </li>
      <li>
        <a href="/iroche/IRO11DB008">/iroche/IRO11DB008</a>
      </li>
      <li>
        <a href="/iroche/IRO11DB008/android">/iroche/IRO11DB008/android</a>
      </li>
      <li>
        <a href="/iroche/IRO12AL004">/iroche/IRO12AL004</a>
      </li>
      <li>
        <a href="/iroche/IRO12AL004/android">/iroche/IRO12AL004/android</a>
      </li>
      <li>
        <a href="/iroche/IRO12BK017">/iroche/IRO12BK017</a>
      </li>
      <li>
        <a href="/iroche/IRO12BK017/android">/iroche/IRO12BK017/android</a>
      </li>
      <li>
        <a href="/iroche/IRO12CK017">/iroche/IRO12CK017</a>
      </li>
      <li>
        <a href="/iroche/IRO12CK017/android">/iroche/IRO12CK017/android</a>
      </li>
      <li>
        <a href="/iroche/IRO13DO056">/iroche/IRO13DO056</a>
      </li>
      <li>
        <a href="/iroche/IRO13DO056/android">/iroche/IRO13DO056/android</a>
      </li>
      <li>
        <a href="/iroche/IRO13FO045">/iroche/IRO13FO045</a>
      </li>
      <li>
        <a href="/iroche/IRO13FO045/android">/iroche/IRO13FO045/android</a>
      </li>
      <li>
        <a href="/iroche/IRO14AH024">/iroche/IRO14AH024</a>
      </li>
      <li>
        <a href="/iroche/IRO14AH024/android">/iroche/IRO14AH024/android</a>
      </li>
      <li>
        <a href="/iroche/IRO14BH024">/iroche/IRO14BH024</a>
      </li>
      <li>
        <a href="/iroche/IRO14BH024/android">/iroche/IRO14BH024/android</a>
      </li>
      <li>
        <a href="/iroche/IRO15BO042">/iroche/IRO15BO042</a>
      </li>
      <li>
        <a href="/iroche/IRO15BO042/android">/iroche/IRO15BO042/android</a>
      </li>
      <li>
        <a href="/iroche/IRO15DO044">/iroche/IRO15DO044</a>
      </li>
      <li>
        <a href="/iroche/IRO15DO044/android">/iroche/IRO15DO044/android</a>
      </li>
      <li>
        <a href="/iroche/IRO16AB012">/iroche/IRO16AB012</a>
      </li>
      <li>
        <a href="/iroche/IRO16AB012/android">/iroche/IRO16AB012/android</a>
      </li>
      <li>
        <a href="/iroche/IRO16BB012">/iroche/IRO16BB012</a>
      </li>
      <li>
        <a href="/iroche/IRO16BB012/android">/iroche/IRO16BB012/android</a>
      </li>
      <li>
        <a href="/iroche/IRO18AO061">/iroche/IRO18AO061</a>
      </li>
      <li>
        <a href="/iroche/IRO18AO061/android">/iroche/IRO18AO061/android</a>
      </li>
      <li>
        <a href="/iroche/IRO22AK021">/iroche/IRO22AK021</a>
      </li>
      <li>
        <a href="/iroche/IRO22AK021/android">/iroche/IRO22AK021/android</a>
      </li>
      <li>
        <a href="/tolix/chair">/tolix/chair</a>
      </li>
      <li>
        <a href="/tolix/chair/android">/tolix/chair/android</a>
      </li>
      <li>
        <a href="/tolix/chair/android/model">/tolix/chair/android/model</a>
      </li>
      <li>
        <a href="/tolix/chair/android/mint">/tolix/chair/android/mint</a>
      </li>
      <li>
        <a href="/tolix/chair/android/pink">/tolix/chair/android/pink</a>
      </li>
      <li>
        <a href="/tolix/chair/android/white">/tolix/chair/android/white</a>
      </li>
      <li>
        <a href="/tolix/chair/android/metal">/tolix/chair/android/metal</a>
      </li>
      <li>
        <a href="/404/">/404/</a>
      </li>
      <li>
        <a href="/dev-catalog/">/dev-catalog/</a>
      </li>
      <li>
        <a href="/">/</a>
      </li>
      <li>
        <a href="/viewer-help/">/viewer-help/</a>
      </li>
      <li>
        <a href="/404.html">/404.html</a>
      </li>
    </ul>
  );
};

export default devCatalog;
