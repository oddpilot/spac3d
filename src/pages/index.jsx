import React from 'react';
import Helmet from 'react-helmet';

class Index extends React.Component {
  render() {
    return (
      <body>
        <Helmet>
          <meta httpEquiv="Refresh" content="0.001; url=https://oddpilot.com" />
          <script>window.location.replace("https://oddpilot.com");</script>
        </Helmet>
        <h1>Hello World!</h1>
        <p>Redirecting you now...</p>
      </body>
    );
  }
}

export default Index;
