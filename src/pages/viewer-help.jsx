import React from 'react';
import Helmet from 'react-helmet';
import Layout from '../layout';

export default () => {
  return (
    <Layout>
      <Helmet title="웹 뷰어 첨부 방법">
        <link rel="stylesheet" href="/css/viewer-help.css" />
      </Helmet>
      <div className="help-container">
        <h1>웹 뷰어 첨부하기</h1>
        <h2>
          웹 뷰어는 iframe HTML 태그를 사용하여 어떤 웹사이트에든 첨부할 수 있습니다. 첨부를 원하는 웹사이트의 HTML
          에디터에서 iframe 태그를 복사해서 붙여넣어보세요.
        </h2>
        <h1>웹 뷰어 크기 설정하기</h1>
        <h2>
          웹 뷰어의 가로 길이를 변경하려면 iframe 태그에서 width, 세로 길이를 변경하려면 height 값을 변경해보세요.
        </h2>
      </div>
    </Layout>
  );
};
