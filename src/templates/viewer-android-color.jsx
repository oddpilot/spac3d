import React from 'react';
import Helmet from 'react-helmet';

const ViewerAndroidPage = ({
  pageContext: {
    productName,
    productCode,
    brandName,
    brandCode,
    colorCode,
    model: { format },
  },
}) => {
  return (
    <div style={{ margin: 0, overflow: 'hidden', position: 'fixed', height: '100%', width: '100%' }}>
      <Helmet title={`${brandName} - ${productName} | Spac3D`}>
        <script type="module" src="https://unpkg.com/@google/model-viewer@0.5.1/dist/model-viewer.js" />
        <script src="https://unpkg.com/fullscreen-polyfill@1.0.2/dist/fullscreen.polyfill.js" />
        <script nomodule src="https://unpkg.com/@google/model-viewer@0.5.1/dist/model-viewer-legacy.js" />
      </Helmet>
      <model-viewer
        style={{ width: '100%', height: '100%' }}
        ar
        alt={productName}
        src={`/models/${brandCode}/${productCode}/${colorCode}${format}`}
        quick-look-browsers="safari chrome"
        auto-rotate
        camera-controls
      />
    </div>
  );
};

export default ViewerAndroidPage;
