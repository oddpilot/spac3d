import React from 'react';
import Helmet from 'react-helmet';

const ViewerAndroidPage = ({
  pageContext: {
    productName,
    productCode,
    brandName,
    brandCode,
    model: { format },
  },
}) => {
  return (
    <div style={{ margin: 0, overflow: 'hidden', position: 'fixed', height: '100%', width: '100%' }}>
      <Helmet title={`${brandName} - ${productName} | Spac3D`}>
        <script type="module" src="https://unpkg.com/@google/model-viewer/dist/model-viewer.js" />
        <script src="https://unpkg.com/fullscreen-polyfill/dist/fullscreen.polyfill.js" />
        <script nomodule src="https://unpkg.com/@google/model-viewer/dist/model-viewer-legacy.js" />
      </Helmet>
      <model-viewer
        /**
         src attribute hotfix: 
         - remade iroche models without shadows but not for other models;
         - add model-no-shadow.gltf files for iroche models that do not have baked-in shadows
         - for other models, add model-no-shadow.glb/gltf files which are duplicates of the original glb/gltf files
         */ 
        src={`/models/${brandCode}/${productCode}/model-no-shadow${brandCode === "iroche" ? ".gltf" : format}`}
        alt={productName}
        ar
        auto-rotate
        camera-controls
        exposure="0.5"
        shadow-intensity="0.4"
        style={{ width: '100%', height: '100%' }}
        quick-look-browsers="safari chrome"
      />
    </div>
  );
};

export default ViewerAndroidPage;
