import React from 'react';
import Helmet from 'react-helmet';
import Layout from '../layout';
import Viewer from '../components/Viewer/Viewer';

const ViewerPage = ({ pageContext }) => {
  return (
    <Layout>
      <Helmet title={`${pageContext.brandName} - ${pageContext.productName} | Spac3D`} />
      <Viewer product={pageContext} />
    </Layout>
  );
};

export default ViewerPage;
